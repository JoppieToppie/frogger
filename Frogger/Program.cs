﻿using System;

namespace Frogger
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        private static Frogger _game;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new Frogger())
            {
                _game = game;
                game.Run();
            }
        }

        public static void Exit()
        {
            _game.Exit();
        }
    }
#endif
}
