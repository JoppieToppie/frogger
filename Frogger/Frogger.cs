﻿using Frogger.GameStates;
using Microsoft.Xna.Framework;

namespace Frogger
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    class Frogger : GameEnvironment
    {
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            screen = new Point(520, 300);

            ApplyResolutionSettings();

            gameStateList.Add(new StartState());
            gameStateList.Add(new PlayingState());
            gameStateList.Add(new WinState());
            gameStateList.Add(new GameOverState());
            
            SwitchTo(0);
        }
    }
}
