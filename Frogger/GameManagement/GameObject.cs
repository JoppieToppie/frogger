﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

class GameObject
{
    public Vector2 position;
    public Vector2 velocity;
    public Texture2D texture;

    public GameObject(string assetName)
    {
        texture = GameEnvironment.ContentManager.Load<Texture2D>(assetName);
        Reset();
    }

    public virtual void Update() { }

    public virtual void Draw(SpriteBatch spriteBatch)
    {
        spriteBatch.Draw(texture, position, Color.White);
    }

    public virtual void Reset()
    {

    }

    public bool Overlaps(GameObject other)
    {
        float w0 = texture.Width,
            h0 = texture.Height,
            w1 = other.texture.Width,
            h1 = other.texture.Height,
            x0 = position.X,
            y0 = position.Y,
            x1 = other.position.X,
            y1 = other.position.Y;

        return !(x0 > x1 + w1 || x0 + w0 < x1 || y0 > y1 + h1 || y0 + h0 < y1);
    }
}
