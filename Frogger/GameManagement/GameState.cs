﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Frogger.GameManagement
{
    abstract class GameState
    {
        protected readonly List<GameObject> gameObjectList;

        public GameState()
        {
            gameObjectList = new List<GameObject>();
        }

        public virtual void Init()
        {
            foreach (var gameObject in gameObjectList)
            {
                gameObject.Reset();
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (var gameObject in gameObjectList)
            {
                gameObject.Update();
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (var gameObject in gameObjectList)
            {
                gameObject.Draw(spriteBatch);
            }
        }
    }
}
