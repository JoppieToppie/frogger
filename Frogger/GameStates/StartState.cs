﻿using Frogger.GameManagement;
using Microsoft.Xna.Framework;

namespace Frogger.GameStates
{
    class StartState : GameState
    {
        public StartState()
        {
            gameObjectList.Add(new GameObject("spr_start_screen"));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // if any key is pressed
            if (Keyboard.GetState().GetPressedKeys().Length > 0)
            {
                // start playing game state
                GameEnvironment.SwitchTo(1);
            }
        }
    }
}
