﻿using Frogger.GameManagement;
using Microsoft.Xna.Framework;

namespace Frogger.GameStates
{
    class GameOverState : GameState
    {
        public GameOverState()
        {
            gameObjectList.Add(new GameObject("spr_gameover_screen"));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // if any key is pressed
            if (Keyboard.GetState().GetPressedKeys().Length > 0)
            {
                // exit the game
                Program.Exit();
            }
        }
    }
}
