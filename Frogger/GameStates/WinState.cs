﻿using Frogger.GameManagement;
using Microsoft.Xna.Framework;

namespace Frogger.GameStates
{
    class WinState : GameState
    {
        public WinState()
        {
            gameObjectList.Add(new GameObject("spr_win_screen"));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // if any key is pressed
            if (Keyboard.GetState().GetPressedKeys().Length > 0)
            {
                // exit the game
                Program.Exit();
            }
        }
    }
}
