﻿using Frogger.GameManagement;
using Frogger.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Frogger.GameStates
{
    class PlayingState : GameState
    {
        private readonly Frog _frog;

        public PlayingState()
        {
            gameObjectList.Add(new GameObject("spr_background"));
            gameObjectList.Add(new Fly("spr_fly", new Vector2(242, 30)));

            _frog = new Frog();
            gameObjectList.Add(_frog);

            SpawnCars();
        }

        private void SpawnCars()
        {
            SpawnTrucks();
            SpawnDozers();
            SpawnRaceCars();
        }

        private void SpawnTrucks()
        {
            gameObjectList.Add(new Car("spr_truck", new Vector2(70, 225), new Vector2(-2, 0)));
            gameObjectList.Add(new Car("spr_truck", new Vector2(220, 225), new Vector2(-2, 0)));
            gameObjectList.Add(new Car("spr_truck", new Vector2(370, 225), new Vector2(-2, 0)));
        }

        private void SpawnDozers()
        {
            gameObjectList.Add(new Car("spr_dozer", new Vector2(70, 185), new Vector2(2, 0)));
            gameObjectList.Add(new Car("spr_dozer", new Vector2(220, 185), new Vector2(2, 0)));
            gameObjectList.Add(new Car("spr_dozer", new Vector2(370, 185), new Vector2(2, 0)));
        }

        private void SpawnRaceCars()
        {
            gameObjectList.Add(new Car("spr_racecar", new Vector2(70, 145), new Vector2(-2, 0)));
            gameObjectList.Add(new Car("spr_racecar", new Vector2(220, 145), new Vector2(-2, 0)));
            gameObjectList.Add(new Car("spr_racecar", new Vector2(370, 145), new Vector2(-2, 0)));
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            DrawLives(spriteBatch);
        }

        private void DrawLives(SpriteBatch spriteBatch)
        {
            for (var i = 0; i < _frog.currentLives; i++)
            {
                spriteBatch.Draw(_frog.texture, new Vector2(10 + i * (_frog.texture.Width + 5), 10), Color.White);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach (var gameObject in gameObjectList)
            {
                if (gameObject is Car)
                {
                    if (_frog.Overlaps(gameObject))
                    {
                        _frog.Reset();
                    }
                }
                else if(gameObject is Fly)
                {
                    if (_frog.Overlaps(gameObject))
                    {
                        GameEnvironment.SwitchTo(2);
                    }
                }
            }
        }
    }
}
