﻿using Microsoft.Xna.Framework;

namespace Frogger.GameObjects
{
    class Fly : GameObject
    {
        public Fly(string assetName, Vector2 spawnPosition) : base(assetName)
        {
            position = spawnPosition;
        }
    }
}
