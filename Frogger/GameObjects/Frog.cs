﻿using Microsoft.Xna.Framework.Input;
using Keyboard = Frogger.GameManagement.Keyboard;

namespace Frogger.GameObjects
{
    class Frog : GameObject
    {
        public int currentLives;

        private const int MaxLives = 3;
        private const int MovementSpeed = 40;

        public Frog() : base("spr_frog")
        {
            currentLives = MaxLives;
        }

        public override void Reset()
        {
            base.Reset();

            position.X = GameEnvironment.Screen.X/2 - texture.Width / 2;
            position.Y = GameEnvironment.Screen.Y - texture.Height;

            currentLives--;
        }

        public override void Update()
        {
            base.Update();

            HandleMovement();

            if (currentLives <= 0)
            {
                GameEnvironment.SwitchTo(3);
            }
        }

        private void HandleMovement()
        {
            Keyboard.GetState();

            if (Keyboard.HasBeenPressed(Keys.Up) && position.Y > MovementSpeed)
            {
                position.Y -= MovementSpeed;
            }

            if (Keyboard.HasBeenPressed(Keys.Down) && position.Y < GameEnvironment.Screen.Y - MovementSpeed)
            {
                position.Y += MovementSpeed;
            }

            if (Keyboard.HasBeenPressed(Keys.Left) && position.X > MovementSpeed)
            {
                position.X -= MovementSpeed;
            }

            if (Keyboard.HasBeenPressed(Keys.Right) && position.X < GameEnvironment.Screen.X - MovementSpeed)
            {
                position.X += MovementSpeed;
            }
        }
    }
}
