﻿using Microsoft.Xna.Framework;

namespace Frogger.GameObjects
{
    class Car : GameObject
    {
        public Car(string assetName, Vector2 spawnPosition, Vector2 spawnVelocity) : base(assetName)
        {
            position = spawnPosition;
            velocity = spawnVelocity;
        }

        public override void Update()
        {
            base.Update();

            position += velocity;

            if (position.X > GameEnvironment.Screen.X) // moving right
            {
                position.X = -texture.Width;
            }
            else if(position.X < -texture.Width) // moving left
            {
                position.X = GameEnvironment.Screen.X;
            }
        }
    }
}
